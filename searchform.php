<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label>
        <span class="screen-reader-text">Search for:</span>
        <input type="search" class="search-field" placeholder="Search …" value="<?php the_search_query() ?>" name="s">
    </label>
    <!--<input type="submit" class="search-submit" value="Search">-->
    <button class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></button>
</form>