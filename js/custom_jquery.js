"use strict";
// var height_window = jQuery(window).height() / 2;
// jQuery(".container-social-menu").css({
//     top: height_window + 'px'
// });
jQuery(".social-toggle").click(function () {
    jQuery('.container-social-menu').toggleClass("active");
});

jQuery(".sticky-bar .search-icon-sticky").click ( function () {
    jQuery(".sticky-bar").toggleClass('active');
});

jQuery("a[href='#']").click ( function () {
    return false;
});
jQuery(".menu-item-has-children > a").click ( function () {
    jQuery(this).parent().toggleClass('focus');
});
jQuery(".search-form .search-icon").click ( function () {
    jQuery(this).children("i").toggleClass('fa-search');
    jQuery(this).children("i").toggleClass('fa-circle-o-notch');
    jQuery(this).children("i").toggleClass('fa-spin');
});
// jQuery(".sticky-bar.active i").click ( function () {
//     var search = jQuery(".sticky-bar .search-form .search-field").value;
//     if ( search != "" ) {
//         alert();
//     }
// });

jQuery(window).scroll(function () {
  if ( jQuery(this).scrollTop() > 200 && !jQuery('.sticky-bar').hasClass('fix') ) {
    jQuery('.sticky-bar').addClass('fix');
    // jQuery('.sticky-bar').slideDown();
   } else if ( jQuery(this).scrollTop() <= 200 ) {
    jQuery('.sticky-bar').removeClass('fix');
    // jQuery('.sticky-bar').slideUp();
  }
});
jQuery(document).ready(function() {
  jQuery(".search-form .search-icon-sticky").click(function() {
    jQuery(this).closest("form").submit();
  });

  jQuery(".widget_pages li.page_item_has_children > a").click(function () {
    jQuery(this).parent().toggleClass("focus");
  });
});