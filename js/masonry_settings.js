jQuery(document).ready(function ($) {

    var $container = $('#footer-sidebar');
    var $masonryOn;
    var $columnWidth = 300;

    if ($(document).width() > 800) {;
        $masonryOn = true;
        runMasonry();
    };

    $(window).resize( function () {
        if ($(document).width() < 800) {
            if ($masonryOn) {
                $container.masonry('destroy');
                $masonryOn = false;
            }
        } else {
            $masonryON = true;
            runMasonry();
        }
    });


    function runMasonry () {
        $($container).masonry({
            columnWidth: $columnWidth,
            itemSelector: '.widget',
            isFitWidth: true,
            isAnimated: true,
            gutter: 10,
        });
    }
});
jQuery(window).scroll(function () {
    if ( jQuery(this).scrollTop() > 200 && !jQuery('.sticky-bar').hasClass('fix') ) {
        jQuery('.sticky-bar').addClass('fix');
        // jQuery('.sticky-bar').slideDown();
    } else if ( jQuery(this).scrollTop() <= 200 ) {
        jQuery('.sticky-bar').removeClass('fix');
        // jQuery('.sticky-bar').slideUp();
    }
});