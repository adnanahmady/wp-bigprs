<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bigprs
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
			<div class="entry-meta">
				<?php bigprs_posted_on(); ?>
			</div><!-- .entry-meta -->
		<?php
		if (has_post_thumbnail()) : ?>
			<div class="single-post-thumbnails clear">
				<?php the_post_thumbnail('large_thumb'); ?>
			</div>
		<?php endif; ?>
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'صفحات :', 'bigprs' ),
				'after'  => '</div>',
			) );
		?>

		<?php if ( 'post' === get_post_type() ||  get_edit_post_link() ) : ?>
			<footer class="entry-footer">
				<?php bigprs_entry_footer(); ?>
			</footer><!-- .entry-footer -->
		<?php endif;  ?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
