<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bigprs
 */

?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'ای بابا، هیچ مطلبی مثل ', 'bigprs');
		 echo '<span>';
		 the_search_query();
		 echo '</span>';
		 esc_html_e(' پیدا نشد.', 'bigprs' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'آماده ای تا اولین مطلبت رو منتشر کنی? <a href="%1$s">از اینجا شروع کن</a>.', 'bigprs' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e( 'رفیق چیزی مثل اونی که تو میخواستی پیدا نکردم، لطفا از کلمات دیگه ای استفاده بکن شاید پیدا شد.', 'bigprs' ); ?></p>
			<?php
				get_search_form();

		else : ?>

			<p><?php esc_html_e( 'به نظر می رسه نمیتونم چیزی رو که تو دنبالشی پیدا بکنم. به احتمال زیاد جستجو می تونه کمکت بکنه.', 'bigprs' ); ?></p>
			<?php
				get_search_form();
		endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
