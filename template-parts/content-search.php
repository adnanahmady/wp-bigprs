<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bigprs
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php bigprs_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
			<a href="<?php the_permalink(); ?>" class="continue-link">ادامه مطلب</a>
		</div><!-- .entry-summary -->

		<?php if ( 'post' === get_post_type() ) : ?>
		<footer class="entry-footer">
			<?php bigprs_entry_footer(); ?>
		</footer><!-- .entry-footer -->
		<?php endif;  ?>
	</div>
</article><!-- #post-## -->
