<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bigprs
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
			// translators: used between list items, there is a space after the comma
			// $category_list = get_the_category_list( __( ', ', 'my-simone' ) );
			/*
			if ( bigprs_categorized_blog() ) {
				echo '<div class="category-list">' . $category_list . '</div>';
			}
			*/
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
			if ( is_sticky() ) :
				echo '<i class="fa fa-thumb-tack" aria-hidden="true"></i>' ;
			endif;
		?>
	</header><!-- .entry-header -->

		<div class="entry-content">
			<div class="entry-meta">
				<?php bigprs_posted_on(); ?>
			</div><!-- .entry-meta -->
		<?php
	 if (has_post_thumbnail()) : ?>
		<div class="single-post-thumbnails clear">
        	<?php the_post_thumbnail('larg_thumb'); ?>
		</div>
    <?php endif; ?>
		<?php
		
			the_content( sprintf(
		
				/* translators: %s: Name of current post. */
				wp_kses( __( 'ادامه مطلب %s <span class="meta-nav">&rarr;</span>', 'bigprs' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'صفحات: ', 'bigprs' ),
				'after'  => '</div>',
			) );
			?>
		<?php if ( 'post' === get_post_type() ) : ?>
		<footer class="entry-footer">
			<?php bigprs_entry_footer(); ?>
		</footer><!-- .entry-footer -->
		<?php endif;  ?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
