<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package bigprs
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="<?php if ( is_404() ) { echo 'error-404'; } else { echo 'no-results'; } ?> not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'ای بابا، این صفحه پیدا نشد.', 'bigprs' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'به نظر میاد هیچی اینجا پیدا نشد. شاید مطالب زیر برات جالب باشه، یا بخوای چیزی که میخوای رو تو مطالب بوسیله این فرم تو سایت جستجو بکنی.', 'bigprs' ); ?></p>

					<?php
						get_search_form();


						// Only show the widget if site has multiple categories.
						// if ( bigprs_categorized_blog() ) :
					?>

					<!--<div class="widget widget_categories">
						<h2 class="widget-title"><?php /* esc_html_e( 'Most Used Categories', 'bigprs' ); */ ?></h2>
						<ul>-->
						<?php
						/*********************************************
						**********************************************
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
						**********************************************
						*********************************************/
						?>
						<!--</ul>
					</div> .widget -->

					<?php
						// endif;

						/* translators: %1$s: smiley */
						/*********************************************
						**********************************************
						$archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'bigprs' ), convert_smilies( ':)' ) ) . '</p>';
						the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );

						the_widget( 'WP_Widget_Tag_Cloud' );
						**********************************************
						*********************************************/
					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->
			<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
