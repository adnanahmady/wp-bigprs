<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package bigprs
 */

if ( ! function_exists( 'bigprs_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function bigprs_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( ' %s', 'post date', 'bigprs' ),
		'<i class="fa fa-cloud-upload" aria-hidden="true"></i> <a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( ' %s', 'post author', 'bigprs' ),
		'<span class="author vcard"><i class="fa fa-user-circle-o" aria-hidden="true"></i> <a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="byline"> ' . $byline . '</span><span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'bigprs_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function bigprs_entry_footer() {
	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( '%s ویرایش', 'bigprs' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>',
		'</span>'
	);
	// Hide category and tag text for pages.
	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link"><i class="fa fa-comments-o" aria-hidden="true"></i> ';
		/* translators: %s: post title */
		comments_popup_link( sprintf( wp_kses( __( 'نظر بدید<span class="screen-reader-text"> درباره %s</span>', 'bigprs' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
		echo '</span>';
	}
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list();
		if ( $categories_list && bigprs_categorized_blog() ) {
			printf( '<span class="cat-links">دسته ها' . esc_html__( ' %1$s', 'bigprs' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		// $tags_list = get_the_tag_list( '', esc_html__( ' ', 'bigprs' ) );
		$tags_list = get_the_tag_list( '<ul><li>', '</li><li>', '</li></ul>' );
		if ( $tags_list ) {
			printf( '<span class="tags-links">برچسب ها' . esc_html__( ' %1$s', 'bigprs' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}


}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function bigprs_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'bigprs_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'bigprs_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so bigprs_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so bigprs_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in bigprs_categorized_blog.
 */
function bigprs_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'bigprs_categories' );
}
add_action( 'edit_category', 'bigprs_category_transient_flusher' );
add_action( 'save_post',     'bigprs_category_transient_flusher' );


function bigprs_social_menu () {
	if ( has_nav_menu ( 'Social' ) ) {
		wp_nav_menu (
			array (
				'theme_location' => 'Social',
				'container' => 'div',
				'container_id' => 'menu-social',
				'container_class' => 'menu-social',
				'menu_id' => 'menu-social-item',
				'menu_class' => 'menu-item',
				'link_before' => '<span class="screen-reader-text">',
				'link_after' => '</span>',
				'depth' => 1,
				'fallback_cb' => '',
			)
		);
	}
}

function bigprs_footer_menu () {
	if ( has_nav_menu ( 'footer-menu' ) ) {
		wp_nav_menu (
			array (
				'theme_location' => 'footer-menu',
				'container' => 'div',
				'container_id' => 'footer-menu',
				'container_class' => 'footer-menu',
				'menu_id' => 'footer-menu-items',
				'menu_class' => 'footer-menu-items',
				'depth' => 1,
				'fallback_cb' => '',
			)
		);
	}
}

function bigprs_horizental_menu () {
	if ( has_nav_menu ( 'horizental-menu' ) ) {
		wp_nav_menu (
			array (
				'theme_location' => 'horizental-menu',
				'container_id' => 'horizental-menu',
				'container_class' => 'horizental-menu',
			)
		);
	}
}