<?php
/**
 * bigprs functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package bigprs
 */

if ( ! function_exists( 'bigprs_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function bigprs_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on bigprs, use a find and replace
	 * to change 'bigprs' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'bigprs', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
        add_image_size('larg_thumb', 1200, 700, true);
        add_image_size('image_thumb', 500, 750, true);

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'bigprs' ),
		'Social' => esc_html__( 'Social Menu', 'bigprs' ),
		'footer-menu' => esc_html__( 'Footer Menu', 'bigprs' ),
		'horizental-menu' => esc_html__( 'Horizental Menu', 'bigprs' ),
	) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		/**
		 * declaring the top short menu of the top of the heading
		 * 
		 * other locations: 
		 * > template-tags.php
		 * > header.php
		 * 
		 */
		'imenu'		=> esc_html__( 'Important Menu', 'bigprs_shop' ),
		'menu-1'	=> esc_html__( 'Primary', 'bigprs_shop' ),
		'footer-menu' => esc_html__( 'Footer Menu', 'bigprs' ),
	) );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'bigprs_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 50,
		'width'       => 50,
		'flex-width'  => true,
		'flex-height' => true,
		// 'header-text' => array( 'site-title', 'site-description', 'sidebar-site-icon' ),
	) );
}
add_image_size ( 'custom-logo', 32, 32 ) ;
endif;
add_action( 'after_setup_theme', 'bigprs_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bigprs_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bigprs_content_width', 650 );
}
add_action( 'after_setup_theme', 'bigprs_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bigprs_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'bigprs_shop' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'bigprs_shop' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<p class="widget-title h2 prefix">',
		'after_title'   => '</p>',
	) );
    register_sidebar( array(
        'name'          => esc_html__( 'footer Sidebar', 'bigprs_shop' ),
        'id'            => 'sidebar-footer',
        'description'   => esc_html__( '"Sidbar on footer" Add widgets here.', 'bigprs_shop' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<span class="widget-title h3">',
        'after_title'   => '</span>',
    ) );
}
add_action( 'widgets_init', 'bigprs_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function bigprs_scripts() {
	//	wp_enqueue_style('bigprs_shop-fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	
		wp_enqueue_style( 'bigprs_shop-local-fontawesome', 'http://localhost/test/font-awesome.min.css', [], '2');
	
		wp_enqueue_style( 'bigprs_shop-fonts', get_template_directory_uri() . '/css/font.css' );
	
		wp_enqueue_style( 'bigprs_shop-style', get_stylesheet_uri() , [],'30');
	
		wp_enqueue_style( 'bigprs_shop-layout-style', get_template_directory_uri() . '/layouts/sidebar-content.css', [], '3' );
	
	//    wp_enqueue_script( 'jquery' );
	
		wp_enqueue_script( 'bigprs_shop-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '2015121504', true );
	
		wp_enqueue_script( 'bigprs_shop-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	
		wp_enqueue_script( 'bigprs_shop-superfish', get_template_directory_uri() . '/js/superfish.min.js', array(), '20171911', true );
	
		wp_enqueue_script( 'bigprs_shop-javascript', get_template_directory_uri() . '/js/bigprs-javascript.js', array('jquery'), '2017112715', true );
	
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
		///////////////////
	if (is_page_template('page-templates/page-nosidebar.php')){
		wp_enqueue_style( 'bigprs-sidebar-content', get_template_directory_uri() . '/layouts/no-sidebar.css?ver=1.0' );
	} else {
		wp_enqueue_style( 'bigprs-sidebar-content', get_template_directory_uri() . '/layouts/sidebar-content.css?ver=1.1' );
	}
	wp_enqueue_style( 'bigprs-fonts', get_template_directory_uri() . '/css/fonts.css?ver=1.1' );
	wp_enqueue_style( 'bigprs-style', get_stylesheet_uri()  . '?ver=1.1');
	wp_enqueue_style( 'bigprs-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css?ver=4.7.0' );
	wp_enqueue_style( 'bigprs-rtl', get_template_directory_uri() . '/rtl.css?ver=1.0' );

	wp_enqueue_script( 'bigprs-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'bigprs-superfish', get_template_directory_uri() . '/js/superfish.min.js', array('jquery'), '20170122', true );
	wp_enqueue_script( 'bigprs-superfish-settings', get_template_directory_uri() . '/js/superfish.settings.js', array('bigprs-superfish'), '20170123', true );
	wp_enqueue_script( 'bigprs-custom-jquery', get_template_directory_uri() . '/js/custom_jquery.js', array('jquery'), '20170124', true );
	wp_enqueue_script( 'bigprs-masonry-settings', get_template_directory_uri() . '/js/masonry_settings.js', array('masonry'), '20170124', true );
	
	wp_enqueue_script( 'bigprs-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bigprs_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * register important sidebar
 * 
 */
//Default WordPress
the_post_thumbnail( 'thumbnail' );     // Thumbnail (150 x 150 hard cropped)
the_post_thumbnail( 'medium' );        // Medium resolution (300 x 300 max height 300px)
the_post_thumbnail( 'medium_large' );  // Medium Large (added in WP 4.4) resolution (768 x 0 infinite height)
the_post_thumbnail( 'large' );         // Large resolution (1024 x 1024 max height 1024px)
the_post_thumbnail( 'full' );          // Full resolution (original size uploaded)
 
//With WooCommerce
the_post_thumbnail( 'shop_thumbnail' ); // Shop thumbnail (180 x 180 hard cropped)
the_post_thumbnail( 'shop_catalog' );   // Shop catalog (300 x 300 hard cropped)
the_post_thumbnail( 'shop_single' );

/**
 * in here i import some custom image sizes for use in the theme
 */
the_post_thumbnail( array( 100, 100 ) ); // Other resolutions (height, width)
the_post_thumbnail ( 'bigprs_shop_medium' ) ;
the_post_thumbnail ( 'bigprs_shop_mini_cart' ) ;
add_image_size ( 'bigprs_shop_mini_cart', 50, 50) ;
add_image_size ( 'bigprs_shop_medium', 450, 450 ); // 300 pixels wide (and unlimited height)
the_post_thumbnail ( 'bigprs_shop_recent_posts' ) ;
add_image_size ( 'bigprs_shop_recent_posts', 50, 50 ) ;

function bigprs_shop_change_content ( $content ) {
	
		if ( has_post_thumbnail () ) {
			$content = get_the_post_thumbnail ( null, 'bigprs_shop_medium' ) . $content;
		}
	
		return $content ;
	}
	add_filter ( 'the_content', 'bigprs_shop_change_content' ) ;
	
/**
 * BEGEAN post thumbnail html
 * در این قسمت به تمامی تصاویر پستها لینک به خود پست اضافه می شه
 * 
 */
add_filter( 'post_thumbnail_html', 'bigprs_shop_image_html', 10, 3 );

function bigprs_shop_image_html( $html, $post_id, $post_image_id ) {

 $html = '<span class="post-thumbnail"><a href="' . get_permalink( $post_id ) . '" title="' . esc_attr( get_the_title( $post_id ) ) . '" class="post-thumbnail">' . $html . '</a></span>';
 return $html;

} // END post thumbnail html


function bigprs_comment ($comment, $args, $depth) {
	
    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    <div class="comment-author vcard">
        <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] = 100 ); ?>
        <div class="comment-meta">
			<?php printf( __( '<cite class="fn">%s</cite> <span class="says">میگه: </span>' ), get_comment_author_link() ); ?>
		</div>
		<div class="comment-metadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
			/* translators: 1: date, 2: time */
			printf( __('%1$s در ساعت %2$s'), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(ویرایش)' ), '  ', '' );
			?>
		</div>
    </div>
    <?php if ( $comment->comment_approved == '0' ) : ?>
         <em class="comment-awaiting-moderation"><?php _e( 'عزیزم نظرت در انتظار تاییده.' ); ?></em>
          <br />
    <?php endif; ?>
	<div class="comment-content">
    	<?php comment_text(); ?>
	</div>
    <div class="reply">
        <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
    <?php
}