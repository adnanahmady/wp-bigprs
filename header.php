<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bigprs
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'bigprs' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php /* esc_html_e( 'Primary Menu', 'bigprs' ); */?><i class="fa fa-bars" aria-hidden="true"></i></button>
			<nav id="show-navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
			</nav>
		</nav><!-- #site-navigation -->
		<nav id="container-social-menu" class="container-social-menu">
			<button class="social-toggle" aria-controls="menu-social" aria-expanded="false"><i class="fa fa-users" aria-hidden="true"></i></button>
			<?php bigprs_social_menu () ; ?>
		</nav>
		
		<nav id="sticky-bar" class="sticky-bar"><!-- SITE STIKYE BAR -->
				<button class="search-icon-sticky"><i class="fa fa-search" aria-hidden="true"></i></button>
			<div id="sticky-search-form" class="sticky-search-form">
				<?php get_search_form(); ?>
			</div>
		</nav><!-- END SITE STIKYE BAR-->
			<?php /* if (!( "blank" == get_header_textcolor () ) ) : ?>
					<div class="site-branding header-background-image" style="background: url('<?php echo get_header_image () ?>');">
			<?php else :*/ ?>
					<div class="site-branding">
			<?php
			// endif;
			if ( is_front_page() && is_home() ) : ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php  else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php 
			endif; 

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
			<div class='site-name-show'>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<span class="site-name">Bigprs</span><span class="site-phrase">.com</span>
				</a>
			</div>
		</div><!-- .site-branding -->

			<?php if ( get_header_image () ) : ?>
				<figure class="header-image">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php the_header_image_tag(); ?></a>
				</figure>
			<?php endif; ?>

	</header><!-- #masthead -->
	
	<div id="content" class="site-content">
